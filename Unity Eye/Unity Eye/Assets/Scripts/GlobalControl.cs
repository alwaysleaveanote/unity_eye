﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalControl : MonoBehaviour
{
    public static GlobalControl Instance;

    public float testDuration;
    public float imageDistance;

    public void SetTime(float test)
    {
        testDuration = test;
    }

    public void SetDist(float image)
    {
        imageDistance = image;
    }

    public float GetTime()
    {
        return testDuration;
    }

    public float GetDist()
    {
        return imageDistance;
    }

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
            testDuration = 30f;
            imageDistance = -2.87f;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
}
