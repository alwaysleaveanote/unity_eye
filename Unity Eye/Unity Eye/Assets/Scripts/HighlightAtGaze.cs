﻿
using Tobii.G2OM;
using UnityEngine;

namespace Tobii.XR.Examples
{
    //Monobehaviour which implements the "IGazeFocusable" interface, meaning it will be called on when the object receives focus
    public class HighlightAtGaze : MonoBehaviour, IGazeFocusable
    {
        public float AnimationTime = 0.1f;

        private Renderer _renderer;
        private Color _targetColor;
        private float startTime = 0;
        private float endTime = 0;
        public float totalTime = 0;
        public Color heatColor;
        public bool active = true;

        private SceneController sceneController;

        //The method of the "IGazeFocusable" interface, which will be called when this object receives or loses focus
        public void GazeFocusChanged(bool hasFocus)
        {
            if(active)
            {            //If this object received focus, fade the object's color to highlight color
            if (hasFocus)
            {
                GetHeat();
                //this.GetComponent<MeshRenderer>().enabled = true;
                startTime = Time.time;
                _targetColor = heatColor; 
            }
            //If this object lost focus, fade the object's color to it's original color
            else
            {
                    GetHeat();

                    endTime = Time.time;

                sceneController.AddToGazeList(this.gameObject.name, endTime-startTime);

                totalTime += endTime - startTime;

                _targetColor = heatColor;
                //this.GetComponent<MeshRenderer>().enabled = false;

            }
            }
        }

        private void Start()
        {
            sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();

            heatColor.r = 0;
            heatColor.g = 1;
            heatColor.b = 0;
            heatColor.a = 1;
            _renderer = GetComponent<Renderer>();
            _targetColor = Color.blue;
        }

        private void Update()
        {
            //This lerp will fade the color of the object
            if (_renderer.material.HasProperty(Shader.PropertyToID("_BaseColor"))) // new rendering pipeline (lightweight, hd, universal...)
            {
                _renderer.material.SetColor("_BaseColor", Color.Lerp(_renderer.material.GetColor("_BaseColor"), _targetColor, Time.deltaTime * (1 / AnimationTime)));
            }
            else // old standard rendering pipline
            {
                _renderer.material.color = Color.Lerp(_renderer.material.color, _targetColor, Time.deltaTime * (1 / AnimationTime));
            }
        }

        private void GetHeat()
        {
            if (heatColor.r < 1)
            {
                heatColor.r = 0f + (totalTime * (10*(1/ sceneController.GetTime())));
            }
            else if (heatColor.r >= 1) 
            {
                heatColor.g = 2f - (totalTime * (10 * (1 / sceneController.GetTime())));
            }
        }

        public float GetTime()
        {
            return totalTime;
        }
    }
}
