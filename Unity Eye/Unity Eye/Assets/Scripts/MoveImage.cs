﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//handles moving the image/heatmap, and the cameras so they are closer of further from the VR(main) camera
public class MoveImage : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    GameObject display;
    [SerializeField]
    InputField newDistance;
    [SerializeField]
    Text curDistanceText;
    [HideInInspector]
    public float currentDistance;

    private GlobalControl global;

    

    void Start()
    {
        global = GameObject.Find("GlobalData").GetComponent<GlobalControl>();
        currentDistance = global.GetDist();
        float displayDistance = -1f * (currentDistance + 2.87f);
        curDistanceText.text = "Current Image Distance: " + displayDistance.ToString();

        var currentPosition = display.transform.position;
        display.transform.position = new Vector3(currentPosition.x, currentPosition.y, currentDistance);
    }
    // Update is called once per frame
    void Update()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        display.transform.Translate(0, 0, scroll * -1f, Space.Self);
        currentDistance = display.transform.position.z;
        //distance.value = currentDistance.ToString();
        float displayDistance = -1f * (currentDistance + 2.87f);
        curDistanceText.text = "Current Image Distance: "+displayDistance.ToString();

        float speed = 1.0f;

        var move = new Vector3(-Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        display.transform.position += move * speed * Time.deltaTime;

    }

    public void SetDistance()
    {
        var currentPosition = display.transform.position;
        float newDistConv = -1*(float.Parse(newDistance.text) + 2.87f);
        display.transform.position = new Vector3(currentPosition.x, currentPosition.y, newDistConv);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("here");
    }
}
