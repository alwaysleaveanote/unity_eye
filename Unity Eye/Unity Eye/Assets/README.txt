Many folders(SteamVRn SteamVR_Resources, Tobii, TobiiXR, ViveSR) in this project dont need to bo touched at all and i havenet done anything with them other than import them.
They handle things like the VR and eye tracking aspects of the software.
https://vr.tobii.com/sdk/develop/unity/getting-started/vive-pro-eye/
This link is the resource i used to get everything up and running withg the vive pro and tobii.

The drag and drop folder is from https://github.com/Bunny83/UnityWindowsFileDrag-Drop
It handles the drag and drop feature of this app. I used their ImageExample script as it was perfect for what we wanted to use it for.

In the plugins, you will find Easy Save 3, I used this to handle saving to a spreadsheet. I didn't change anything in this folder.

Prefabs- the cube prefab is the building block of the heatmap prefab.

Scripts
CanvasExtensions- handles the imported image and displaying it with the correct dimensions.
GridSetup- This is useful if in the future, different grids need to be made. For example, if you want a more detailed heatmap, just duplicate the cube prefab adjust it's size, and add the
GridSetup script to the scene, and adjust the values in the script. This will create a new heatmap that you can save as a prefab when you play the scene.
HiResScreenShots- This script handles taking screenshots of the grid when a test finishes.
MoveImage- handles moving the image/heatmap, and the cameras so they are closer of further from the VR(main) camera.
SceneController- handels various UI elements in the scene.

TobiiXR/Examples/Utilities/HighlightAtGaze- handles when a gaze lands on one of the cubes, saves the relevant info, and sets the cubes heatmap color.

